public class PrimitiveArrayExamples {

	public static void main(String[] args) {
		// 1. Create an empty array of String
		// By default, array stores 5 names
		String[] names = new String[5];
		
		// 2. Show the total number of items in array
		System.out.println("Total number of items in array: ");
		// @TODO: Write code to output total num items to array
		System.out.println(names.length);
		System.out.println("----------");
		
		// 3. Add something to array
		names[0] = "Jenelle";
		// @TODO: Write code to add someone to index 3
		names[3] = "Peter";
		
		// 4. Output the item in position 0
		System.out.println("Name in pos 0:");
		// @TODO: Write the code to output the the name in index 0
		System.out.println(names[0]);
		System.out.println("----------");
		
		
		// 5. Output everything in array
		System.out.println("Everything in array: ");
		// @TODO: Write code to output everything
		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}
		System.out.println("----------");
				
		// 6. Add something to the end of the array
		// Add code to handle errors
		try {
			names[7] = "Emad";
			// @TODO:  Write code to output names[7] to screen
			System.out.println(names[7]);
		}
		catch (Exception e) {
			System.out.println("Error when adding Emad to array");
			// use this code to see what the error message is
			System.out.println(e.toString());
			System.out.println("----------");
		}
		
		// 7. Remove someone from the array
		// @TODO: Fill in code here
		System.out.println("Removing someone from array");
		names[3] = null;		// for ints & doubles, set back to 0
		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}	
		System.out.println("----------");
		
		// 8. Loop through every item in the array and output:
		// HELLO _______ (where ___ is the name)
		names[1] = "Mohammad";
		names[2] = "Pritesh";
		names[3] = "Peter";
		names[4] = "Cryston";
				
		// note: index 4 is still empty (null)
		// at this point, your array should be:
		// 	- jenelle, mohammad, pritesh, peter, cryston
		for (int i = 0; i < names.length; i++) {
			System.out.println("HELLO " + names[i]);
		}
		System.out.println("----------");
		
		// 9. Shift everyone by 1 position
		// @TODO: Fill in code here
		// - null, jenelle, mohammad, pritesh, peter
		// Cryston gets dropped
		
		// 0		1		  2		   3      4
		// jenelle, mohammad, pritesh, peter, cryston
		
		String temp;		// temp = cryston
		for (int i = names.length-2; i >= 0; i--) {
			names[i+1] = names[i];
			temp = names[i];
			names[i] = null;
		}
		
		
		// output everyone
		for (int i = 0; i < names.length; i++) {
			System.out.println(names[i]);
		}
		
		
		
		
		
	}
}


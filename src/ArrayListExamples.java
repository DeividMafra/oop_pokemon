import java.util.ArrayList;

public class ArrayListExamples {

	public static void main(String[] args) {
		// 1. Create a new List
		ArrayList<String> names = new ArrayList<String>();
		
		// 1. Add 2 people to the list
		names.add("Jenelle");
		names.add("Peter");
		
		// 2. Output total number of people in list
		System.out.println(names.size());
		
		// adding more names
		names.add("Pritesh");
		names.add("Emad");
		
		// 3. Delete someone from the list
		names.remove(0);
		System.out.println(names.size());
		// 4. Output all people in the list
		
		// OPTION1 : Use a toString();
				System.out.println(names.toString());
				
		// OPTION 2:  Use a for-loop
				for (int i = 0; i < names.size(); i++) {
					System.out.println(names.get(i));
				}
				System.out.println("---------");
		
		// 5. Get one person out of the list
		
				System.out.println(names.get(2));
				System.out.println("---------");
				
		// 6. Change the name of the person
				// Replace Pritesh with Mohammad				
				names.set(1, "Mohammad");
				System.out.println(names.toString());
		
				
				// 8. Loop through every item in the list and output:
				// HELLO _______ (where ___ is the name)
				for (int i = 0; i < names.size(); i++) {
					System.out.println("HELLO " + names.get(i));
				}
				// 7. Delete everyone from list
				names.clear();
	}
}
